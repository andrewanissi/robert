import { cciDir, serviceDir, mobileDir, desktopDir } from '../config';
import { update, build, buildService, buildCommons } from '../helpers/helpers';

if (process.env.UPDATE === 'true') {
	Promise.all([
		Promise.resolve(update('eonline-service', serviceDir)),
		Promise.resolve(update('eonline-commons-client', cciDir, true))
	])
		.then(buildCommons)
		.then(buildService);
} else {
	buildCommons();
	buildService();
}

if (process.argv.includes('--d')) {
	build('Docker Image - Mobile', mobileDir, ['docker build -t mobile .'], true);
	build('Docker Image - Desktop', desktopDir, ['docker build -t desktop .'], true);
}
