![alt text](/images/robertfrost-1910.jpg)

# Robert - A Manner of Building Repositories of Import
Welcome, esteemed developers and engineers, to Robert. I sincerely hope your time with us will be most exquisite. Robert is at your disposal for purposes of building
repositories of import, specifically for EOnline.

Please note, this version of Robert uses a Babel script, allowing
source code to be written in JavaScript ES2015 or later.

## Prerequisites for EOL
1. NODE v6+
2. Maven 3.5.0 is installed and can be called with `mvn` via the command line.

## Installation
Once cloned or forked :
```bash
npm install
```
Then open `src/config.js` and update to point to YOUR directories
```js
export const mtsSageDir = '~/code/mts-sage';
export const mtsCatmanDir = '~/code/mts-catman';
export const mtsTribeDir = '~/code/mts-tribe';
export const mtsCmsDataServiceDir = '~/code/mts-cms-data-service';
export const mtsJbossDir = '~/code/mts-sage-jboss';
export const serviceDir = '~/code/eonline-service';
export const cciDir = '~/code/eonline-commons-client';
export const branchToUpdate = 'dev-branch';
export const remoteName = 'upstream';
```

## To Update and Build Backend for eonline (CCI & Service):
#### Step 1
Make sure eonline-service and eonline-commons-client are both in the git branch that you would like to build.

#### Step 2
 Updates CCI & Service from upstream (config.branchToUpdate) and then builds both
```js
npm start
```
