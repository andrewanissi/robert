/* eslint no-console: 0 */
import shell from 'shelljs';
import { branchToUpdate, remoteName, cciDir, serviceDir } from '../config';
import colors from 'colors';
import currentBranch from 'git-branch';
import simpleGit from 'simple-git';
import simpleGitPromise from 'simple-git/promise';

const branch = branchToUpdate || 'dev-branch';

/**
 * @param name {String} The project name
 * @param dir {String} The project directory
 * @param shouldStash {Boolean} OPTIONAL - Whether it should stash before merge
 */
export async function update(name, dir, shouldStash = false) {
	console.log('currentBranch:', currentBranch.sync(dir));
	console.log(`Updating ${name}`.green, '\n');

	// stash uncommitted changes
	if (shouldStash) {
		const gitCommand = simpleGitPromise(dir);

		console.log('called "git stash"'.magenta);
		return gitCommand
			.stash()
			.then(() => {
				return gitCommand.fetch(remoteName);
			})
			.then(() => {
				return gitCommand.pull(remoteName, branch, (err, gitUpdate) => {
					if (err) {
						throw err;
					} else if (gitUpdate && gitUpdate.summary) {
						console.log(`${name} updates: ${gitUpdate.summary.changes}`.magenta);
					}
				});
			})
			.then(() => {
				return gitCommand.stash(['pop']);
			})
			.then(() => {
				console.log('called "git stash pop"'.magenta);
				console.log('\n', `Finished updating ${name}`.green);
			});
	} else {
		const gitCommand = simpleGit(dir);

		return gitCommand.fetch(remoteName).pull(remoteName, branch, (err, gitUpdate) => {
			if (err) {
				throw err;
			} else if (gitUpdate && gitUpdate.summary) {
				console.log(`${name} updates:`, gitUpdate.summary.changes);
				console.log('\n', `Finished updating ${name}`.green);
			}
		});
	}
}

/**
 * @param name {String} The project name
 * @param dir {String} The project directory
 * @param commands {Array} An array of commands to execute to build
 */
export const build = (name, dir, commands) => {
	console.log(`*************** Building ${name} ***************`.green, '\n');
	shell.cd(dir);

	commands.forEach(command => {
		if (shell.exec(command).code !== 0) {
			shell.echo(`*************** ${name} build failed ***************`);
			shell.exit(1);
		}
	});

	console.log(' ');
};

/**
 * build eonline-commons-client
 */
export function buildCommons() {
	build('eonline-commons-client', cciDir, ['mvn -DskipTests clean remote-resources:bundle install']);
}

/**
 * build eonline-service
 */
export function buildService() {
	build('eonline-service', serviceDir, ['mvn -DskipTests clean remote-resources:bundle install']);
}
